#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "sdkconfig.h"

// Pin numbers for the shift register clock and data
#define CLK 18
#define DATA 19

// Function prototypes
void shiftOut(uint8_t*, int, int, int);

// Main task
void app_main(void)
{
    // Select GPIO functionality for CLK and DATA pins
    gpio_pad_select_gpio(CLK);
    gpio_pad_select_gpio(DATA);

    // Set the GPIO as a push/pull output
    gpio_set_direction(CLK, GPIO_MODE_OUTPUT);
    gpio_set_direction(DATA, GPIO_MODE_OUTPUT);

    // Data to be shifted out, MSB first, byte 0 first
    uint8_t data[3] = {0b11001100, 0b11001100, 0b11000000};

    while(1)
    {
        // invert bits each loop for demo purposes
        for (int i = 0; i < sizeof(data); ++i)
            data[i] = ~data[i];

        shiftOut(&data, sizeof(data), CLK, DATA);
        
        // delay 1000 ms, resume doing other things in larger project
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
}

// shifts out n bytes
void shiftOut(uint8_t *data, int n, int clkPin, int dataPin)
{
    for (int i = 0; i < n; ++i)
    {
        for (int j = 7; j >= 0; --j)
        {
            gpio_set_level(clkPin, 0);
            gpio_set_level(dataPin, data[i] & (1 << j));

            // delay 10 us
            vTaskDelay(0.01 / portTICK_PERIOD_MS);

            gpio_set_level(clkPin, 1);
            
            // delay 10 us
            vTaskDelay(0.01 / portTICK_PERIOD_MS);
        }
    }

    // extra pulse to catch up storage register
    gpio_set_level(clkPin, 0);
    vTaskDelay(0.01 / portTICK_PERIOD_MS);
    gpio_set_level(clkPin, 1);
    vTaskDelay(0.01 / portTICK_PERIOD_MS);
    gpio_set_level(clkPin, 0);
}
